## ThinkCase v1.0.0

ThinkCase是一个基于 ThinkAdmin V5 开发的小型工单管理系统。

开发文档可以参考ThinkAdmin V5开发文档：http://doc.thinkadmin.top/thinkadmin-v5

![avatar](http://q089njetp.bkt.clouddn.com/1.jpg)

![avatar](http://q089njetp.bkt.clouddn.com/2.jpg)

#### 注意事项 
* 项目测试需要自行搭建环境导入数据库( admin_v5.sql )并修改配置( config/database.php )；
* 若操作提示“演示系统禁止操作”等字样，需要删除演示路由配置( route/demo.php )或清空路由文件；
* 当前版本使用 ThinkPHP 5.1.x，对 PHP 版本标注不低于 PHP 5.6，具体请阅读 ThinkPHP 官方文档；
* 环境需开启 PATHINFO，不再支持 ThinkPHP 的 URL 兼容模式运行（源于如何优雅的展示）；

## 技术支持

开发文档：http://doc.thinkadmin.top/thinkadmin-v5

开发前请认真阅读 ThinkPHP 官方文档会对您有帮助哦！

本地开发命令`php think run`，使用`http://127.0.0.1:8000`访问项目。

## 注解权限

注解权限是指通过方法注释来实现后台RBAC授权管理，用注解来管理功能节点。

开发人员只需要写好注释，RBAC的节点会自动生成，只需要配置角色及用户就可以使用RBAC权限。

* 此版本的权限使用注解实现
* 注释必需使用标准的块注释，如下案例
* 其中`@auth true`表示访问需要权限验证
* 其中`@menu true`显示在菜单编辑的节点可选项
```php
/**
* 操作的名称
* @auth true  # 表示需要验证权限
* @menu true  # 在菜单编辑的节点可选项
*/
public function index(){
   // @todo
}
```

## 项目版本
体验账号及密码都是`admin`
